.data
	enter_phrase: .asciiz "Enter phrase (up to 150 characters): "
	phrase_space: .space 152
	enter_key: .asciiz "Enter key(up to 12 lowercase characters): "
	key_space: .space 14
	enter_operation: .asciiz "Enter which operation do you want to perform (C - cipher, D - decipher): "
	operation: .space 3
	once_again: .asciiz "\nDo you want to cipher/decipher another phrase (y/n)?"
	
.text
main:
	jal get_arguments
	jal get_operation
	beq $v0, 67, jump_cipher
	beq $v0, 99, jump_cipher
	beq $v0, 68, jump_decipher
	beq $v0, 100, jump_decipher
	j main
	
	jump_cipher:
		jal cipher
		j end_program
	jump_decipher:
		jal decipher
		j end_program
	
get_arguments:
	#Print text to get first phrase
	la $a0, enter_phrase
	li $v0, 4
	syscall
	#Get phrase from user
	la $a0, phrase_space
	li $a1, 152
	li $v0, 8
	syscall
	
	#Print text to get key
	la $a0, enter_key
	li $v0, 4
	syscall
	#Get key from user
	la $a0 key_space
	li $a1, 14
	li $v0, 8
	syscall
	
	jr $ra
	
get_operation:
	#Print text to get operation
	la $a0, enter_operation
	li $v0, 4
	syscall
	#Get operation String
	la $a0, operation
	li $a1, 3
	li $v0, 8
	syscall
	#Get first character
	li $t0, 0
	lb $v0, operation($t0)
	#If character isn't C, c, D or d then user needs to enter it again
	beq $v0, 67, correct_operation
	beq $v0, 68, correct_operation
	beq $v0, 99, correct_operation
	beq $v0, 100, correct_operation
	j get_operation
	
	correct_operation:		
		jr $ra
	
	
cipher:
	move $t0, $ra
	jal to_lower_case
	jal remove_extras
	jal calc_keys_length
	move $ra, $t0
	move $t0, $v0 #Now we have length of key at $t0
	
	li $t1, 0
	li $t2, 0
	do_while_cipher:
		lb $t3, phrase_space($t1) #get char from phrase
		beqz $t3, end_cipher #if it's not a char then we can end
		lb $t4, key_space($t2) #get char from key
		sub $t3, $t3, 96 #make it between 1 and 26
		sub $t4, $t4, 96 #make it between 1 and 26
		
		add $t3, $t3, $t4 #add key_char to phrase_char
		
		cipher_char_not_ok:
			ble $t3, 26, cipher_char_ok #if it's between 1 and 26 it's ok
			sub $t3, $t3, 26 #otherwise subtract 26, as long as it's too big
			j cipher_char_not_ok
		
		cipher_char_ok:
			add $t3, $t3, 96 #add 96 to the char, so that it's in ascii
			sb $t3, phrase_space($t1) #put it modified on it's place
		
			add $t1, $t1, 1 #increment phrase index
			add $t2, $t2, 1 #increment key index
			
		cipher_key_index_not_ok: #if key index is bigger then key length, start from 0
			blt $t2, $t0, do_while_cipher 
			sub $t2, $t2, $t0
			j cipher_key_index_not_ok
	
	end_cipher:
		jr $ra

decipher:
	move $t0, $ra
	jal to_lower_case
	jal remove_extras
	jal calc_keys_length
	move $ra, $t0
	move $t0, $v0 #Now we have length of key at $t0
	
	li $t1, 0
	li $t2, 0
	do_while_decipher:
		lb $t3, phrase_space($t1) #get char from phrase
		beqz $t3, end_decipher #if it's not a char then we can end
		lb $t4, key_space($t2) #get char from key
		sub $t3, $t3, 96 #make it between 1 and 26
		sub $t4, $t4, 96 #make it between 1 and 26
		
		sub $t3, $t3, $t4 #subtract key_char from phrase_char
		
		decipher_char_not_ok:
			bge $t3, 1, decipher_char_ok #if it's between 1 and 26 it's ok
			add $t3, $t3, 26 #otherwise add 26, as long as it's too small
			j decipher_char_not_ok
		
		decipher_char_ok:
			add $t3, $t3, 96 #add 96 to the char, so that it's in ascii
			sb $t3, phrase_space($t1) #put it modified on it's place
		
			add $t1, $t1, 1 #increment phrase index
			add $t2, $t2, 1 #increment key index
			
		decipher_key_index_not_ok: #if key index is bigger then key length, start from 0
			blt $t2, $t0, do_while_decipher 
			sub $t2, $t2, $t0
			j decipher_key_index_not_ok
	
	end_decipher:
		jr $ra
	
to_lower_case:
	li $t1, 0
	
	do_while_not_lower_case:
		lb $t2, phrase_space($t1) #Load char at index $t1 to $t2
		move $t3, $t1 #Remember what $t1 is at $t3
		add $t1, $t1, 1 #Increment $t1
		beqz, $t2, end_lower_case #When sentence ends, array is full of 0's
		blt $t2, 65, do_while_not_lower_case #Skip if it's not upper case
		bgt $t2, 90, do_while_not_lower_case #Skip if it's not upper case
		
		add $t2, $t2, 32 #Make upper case lower case
		sb $t2, phrase_space($t3) #Save char as lower case at it's correct place
		j do_while_not_lower_case #Next iteration
	
	end_lower_case:
		jr $ra	
	
remove_extras:
	li $t1, 0
	li $t2, 0
	li $t5, 0
	
	do_while_extras:
		lb $t3, phrase_space($t1) #Load char at index $t1 to $t3
		move $t4, $t1 #Remember what $t1 is at $t4
		add $t1, $t1, 1 #Increment $t1
		beqz $t3, clean_up #When sentence ends, array is full of 0's
		blt $t3, 97, do_while_extras #Skip if it's not a character
		bgt $t3, 122, do_while_extras #Skip if it's not a character
		
		sb $t3, phrase_space($t2) #Save char at correct spot, spaces and shit are omitted
		add $t2, $t2, 1 #Increment $t2, since we just saved to this spot
		j do_while_extras #Next iteration
		
	clean_up: #Save zero until zero met, from where we finished, so there are no duplicates
		lb $t6, phrase_space($t2)
		beqz $t6, end_remove_extras
		sb $t5, phrase_space($t2) 
		add $t2, $t2, 1
		j clean_up
	
	end_remove_extras:
		jr $ra


calc_keys_length: #Iterate through key_space as long as values are lower case chars, return number of iterations
	li $t1, 0
	do_while_keys_length:
		lb $t2, key_space($t1)
		blt $t2, 97, end_calc_keys_length
		bgt $t2, 122, end_calc_keys_length
		add $t1, $t1, 1
		j do_while_keys_length
	
	end_calc_keys_length:
		move $v0, $t1
		jr $ra

end_program:
	print_result:
		la $a0, phrase_space
		li $v0, 4
		syscall
			
	wants_once_again:
		la $a0, once_again
		li $v0, 4
		syscall
		#Get operation String
		la $a0, operation
		li $a1, 3
		li $v0, 8
		syscall
		#Get first character
		li $t0, 0
		lb $v0, operation($t0)
		
		beq $v0, 121, main
		beq $v0, 89, main
		beq $v0, 110, end
		beq $v0, 78, end
		j wants_once_again
			
	end:	
		li $v0, 10
		syscall
			
