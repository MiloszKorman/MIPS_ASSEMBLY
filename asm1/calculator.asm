#Created by Miłosz Korman
#Program performs addition, subtraction, multiplication and division on two numbers
#provided by user based on operation number provided by user.
#First number is stored in $t1, second number in $t2, operation number in $t3, 
#result is stored i $t0.

.data
	first_number: .asciiz "Provide first number: "
	second_number: .asciiz "Provide second number: "
	operation: .asciiz "Provide operation number: "
	result: .asciiz "Result: "
	incorrect: .asciiz "Can't divide by 0!"
	again: .asciiz "Do you want to do it again?: "
	new_line: .asciiz "\n"
	
.text
main:
	j get_arguments #start by getting arguments
	
get_arguments:
	#Print text to get first number
	la $a0, first_number
	li $v0, 4
	syscall
	#get first number
	li $v0, 5
	syscall
	move $t1, $v0
	
	#Print text to get second number
	la $a0, second_number
	li $v0, 4
	syscall
	#get second number
	li $v0, 5
	syscall
	move $t2, $v0
	
	j get_operation
	
get_operation:
	#Print text to get operation number
	la $a0, operation
	li $v0, 4
	syscall
	#get operation number
	li $v0, 5
	syscall
	move $t3, $v0
	
	j decide_operation
	
#Either suitable operation is performed, 
#or when operation number is incorrect,
#user is asked to provide it once again
decide_operation:
	beq $t3, 1, addition
	beq $t3, 2, multiplication
	beq $t3, 3, subtraction
	beq $t3, 4, division
	j get_operation

#Operations on numbers, results are stored in $t0
addition:
	add $t0, $t1, $t2
	j print_result
subtraction:
	sub $t0, $t1, $t2
	j print_result
multiplication:
	mul $t0, $t1, $t2
	j print_result
division:
	beqz $t2, incorrect_argument #If second number is 0, 
				#and we try to perform division, we try to divide by 0
	div $t1, $t2
	mflo $t0
	j print_result
	
print_result:
	#Print result text
	la $a0, result
	li $v0, 4
	syscall
	#Print result
	move $a0, $t0
	li $v0, 1
	syscall
	#Print new line
	la $a0, new_line
	li $v0, 4
	syscall
	
	j wants_once_again

#Called when user tries to divide by 0
incorrect_argument:
	#Print information that one of arguments provided was incorrect
	la $a0, incorrect
	li $v0, 4
	syscall
	#Print new line
	la $a0, new_line
	li $v0, 4
	syscall
	
	j wants_once_again

#Find out whether user wants to try again
wants_once_again:
	#Print information whether user wants to do it again
	la $a0, again
	li $v0, 4
	syscall	
	#get answer
	li $v0, 5
	syscall
	move $t3, $v0
	#1 - do it all over again, other - get answer until 1 or 0
	beq $t3, 1, all_over_again
	bnez $t3, wants_once_again
	#0 - end program
	li $v0, 10
	syscall
	
all_over_again:
	#Print 2 new lines
	la $a0, new_line
	li $v0, 4
	syscall
	la $a0, new_line
	li $v0, 4
	syscall
	#start from first label
	j get_arguments
	
