#Created by Miłosz Korman
#Program performs addition, subtraction, multiplication and division on two 
#float numbers provided by user based on operation number provided by user.
#First number is stored in $f2, second number in $f4, operation number in $t0, 
#result is stored i $f6	.

.data
	operation: .asciiz "Provide operation number (1-addition, 2-multiplication, 3-subtraction, 4-division): "
	first_number: .asciiz "Provide first number: "
	second_number: .asciiz "Provide second number: "
	result: .asciiz "Result: "
	incorrect: .asciiz "Can't divide by 0!"
	again: .asciiz "Do you want to do it again (1-yes, 0-no)?: "
	new_line: .asciiz "\n"
	
	zero: .double 0.00
	
.text
main:
get_operation:
	#Print text to get operation number
	la $a0, operation
	li $v0, 4
	syscall
	#get operation number
	li $v0, 5
	syscall
	move $t0, $v0
	
	blt $t0, 1, get_operation
	bgt $t0, 4, get_operation
	
get_arguments:
	#Print text to get first number
	la $a0, first_number
	li $v0, 4
	syscall
	
	#Get first number
	li $v0, 7
	syscall
	
	#Move it to register $f1
	mov.d $f2, $f0
	
	#Print text to get second number
	la $a0, second_number
	li $v0, 4
	syscall
	
	#Get second number
	li $v0, 7
	syscall
	
	#Move it to register $f2
	mov.d $f4, $f0	
	
#Either suitable operation is performed, 
#or when operation number is incorrect,
#user is asked to provide it once again
decide_operation:
	beq $t0, 1, addition
	beq $t0, 2, multiplication
	beq $t0, 3, subtraction
	beq $t0, 4, division
	j get_operation
	
	
addition:
	add.d $f6, $f2, $f4
	j print_result

multiplication:
	mul.d $f6, $f2, $f4
	j print_result

subtraction:
	sub.d $f6, $f2, $f4
	j print_result

division:
	#throw exception when $f2 == 0
	l.d $f8, zero
	c.eq.d $f4, $f8
	bc1t incorrect_argument
	
	div.d $f6, $f2, $f4
	j print_result

print_result:
	#Print result text
	la $a0, result
	li $v0, 4
	syscall
	
	#Print result
	mov.d $f12, $f6
	li $v0, 3
	syscall
	
	#Print new line
	la $a0, new_line
	li $v0, 4
	syscall
	
	j wants_once_again

#Called when user tries to divide by 0
incorrect_argument:
	#Print information that second number can't be 0 in division
	la $a0, incorrect
	li $v0, 4
	syscall
	
	#Print new line
	la $a0, new_line
	li $v0, 4
	syscall
	
	j wants_once_again

#Find out whether user wants to try again
wants_once_again:
	#Print information whether user wants to do it again
	la $a0, again
	li $v0, 4
	syscall	
	
	#get answer
	li $v0, 5
	syscall
	move $t3, $v0
	
	#1 - do it all over again, other - get answer until 1 or 0
	beq $t3, 1, all_over_again
	bnez $t3, wants_once_again
	
	#0 - end program
	li $v0, 10
	syscall
	
all_over_again:
	#Print 2 new lines
	la $a0, new_line
	li $v0, 4
	syscall
	la $a0, new_line
	li $v0, 4
	syscall
	#start from the beginning
	j main
