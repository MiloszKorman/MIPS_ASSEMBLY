.data
	add_instruction: .asciiz "ADD $*, $*, $*"
	addi_instruction: .asciiz "ADDI $*, $*, " 	#+value
	jump_instruction: .asciiz "J " 			#+label
	noop_instruction: .asciiz "NOOP"
	mult_instruction: .asciiz "MULT $*, $*"
	jump_register_instruction: .asciiz "JR $*"
	jump_and_link_instruction: .asciiz "JAL " 	#+label
	
	number_of_instructions_text: .asciiz "How many instructions do you want to provide?: "
	
	provide_text: .asciiz "Provide "
	instruction_text: .asciiz " instruction: "
	
	sentence: .space 18
	
	incorrect_instruction: .asciiz "Incorrect instruction\n"
	correct_instruction: .asciiz "Correct instruction\n"
	new_line: .asciiz "\n"
	space_allocated: .asciiz "\n\nSpace allocated on stack: "
	bytes: .asciiz "B"
	
	stack_operator: .word 1
	
.text
main:
	li $t0, 1
	
	get_number_of_instructions:
		la $a0, number_of_instructions_text
		li $v0, 4
		syscall
		li $v0, 5
		syscall
		add $s0, $v0, 1
		blt $s0, 1, get_number_of_instructions
		bgt $s0, 5, get_number_of_instructions
	
	while_instructions:
		la $a0, provide_text
		li $v0, 4
		syscall
		move $a0, $t0
		li $v0, 1
		syscall
		la $a0, instruction_text
		li $v0, 4
		syscall
		la $a0, sentence
		li $a1, 18
		li $v0, 8
		syscall
		jal check_if_proper
		li $t1, 0
		li $t3, 0
		bne $v0, 1, clean_up
		jal add_whole_instruction_to_stack
		add $t0, $t0, 1
		
		clean_up:
			beq $t0, $s0, end_program
			lb $t2, sentence($t1)
			beqz $t2, while_instructions
			sb $t3, sentence($t1) 
			add $t1, $t1, 1
			j clean_up
	
	end_program:
		jal print_elements
		
		mul $t9, $t9, 4
		
		#Print allocated space
		la $a0, space_allocated
		li $v0, 4
		syscall
		
		move $a0, $t9
		li $v0, 1
		syscall
		
		la $a0, bytes
		li $v0, 4
		syscall
	
		li $v0, 10
		syscall

check_if_proper:
	move $t1, $ra
	jal check_add
	beq $v0, 1, proper
	jal check_addi
	beq $v0, 1, proper
	jal check_jump
	beq $v0, 1, proper
	jal check_noop
	beq $v0, 1, proper
	jal check_mult
	beq $v0, 1, proper
	jal check_jump_register
	beq $v0, 1, proper
	jal check_jump_and_link
	beq $v0, 1, proper
	
	not_proper:
		move $ra, $t1
		la $a0, incorrect_instruction
		li $v0, 4
		syscall
		jr $ra
	
	proper:
		move $ra, $t1
		la $a0, correct_instruction
		li $v0, 4
		syscall
		li $v0, 1
		jr $ra
	
check_add:
	li $t2, 0 #instruction index
	li $t5, 0 #sentence index
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_add:
		lb $t3, sentence($t5)
		lb $t4, add_instruction($t2)
		bne $t3, $t4, check_whether_register_add
		beq $t2, 13, is_add
		add $t2, $t2, 1
		add $t5, $t5, 1
		j while_check_add
	
	#if not, check whether or * is proper register
	check_whether_register_add:
		bne $t4, 42, end_check_add
		
		move $s1, $ra
		move $v0, $t5
		jal check_proper_register
		move $ra, $s1
		beq $v0, 0, end_check_add
		add $t5, $t5, 1
		add $t2, $t2, 1
		lb $t3, sentence($t5)
		beq $t3, 44, while_check_add
		add $t5, $t5, 1
		j while_check_add
			
	is_not_add:
		li $v0, 0
		j end_check_add		
	is_add:
		li $v0, 1
	end_check_add:
		jr $ra

check_addi:
	li $t2, 0
	li $t5, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_addi:
		lb $t3, sentence($t5)
		lb $t4, addi_instruction($t2)
		bne $t3, $t4, check_whether_register_addi
		beq $t2, 12, is_addi
		add $t2, $t2, 1
		add $t5, $t5, 1
		j while_check_addi
		
	#if not, check whether or * is proper register
	check_whether_register_addi:
		bne $t4, 42, end_check_addi
		
		move $s1, $ra
		move $v0, $t5
		jal check_proper_register
		move $ra, $s1
		beq $v0, 0, end_check_addi
		add $t5, $t5, 1
		add $t2, $t2, 1
		lb $t3, sentence($t5)
		beq $t3, 44, while_check_addi
		add $t5, $t5, 1
		j while_check_addi
			
	is_not_addi:
		li $v0, 0
		j end_check_addi	
		
	is_addi:
		#check whether proper value is provided at the end
		move $s1, $ra
		move $v0, $t5
		jal check_only_digits
		move $ra, $s1
		
		beq $v0, 0, end_check_addi
		li $v0, 1
		
	end_check_addi:
		jr $ra
	
check_jump:
	li $t2, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_jump:
		lb $t3, sentence($t2)
		lb $t4, jump_instruction($t2)
		bne $t3, $t4, end_check_jump
		beq $t2, 1, is_jump
		add $t2, $t2, 1
		j while_check_jump
		
	is_jump:
		#check wheter proper label is provided at the end
		move $s1, $ra
		move $v0, $t2
		jal check_label
		move $ra, $s1
		
		beq $v0, 0, end_check_jump
		li $v0, 1
		
	end_check_jump:
		jr $ra

check_noop:
	li $t2, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_noop:
		lb $t3, sentence($t2)
		lb $t4, noop_instruction($t2)
		bne $t3, $t4, end_check_noop
		beq $t2, 3, is_noop
		add $t2, $t2, 1
		j while_check_noop
		
	is_noop:
		li $v0, 1
	end_check_noop:
		jr $ra
	
check_mult:
	li $t2, 0
	li $t5, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_mult:
		lb $t3, sentence($t5)
		lb $t4, mult_instruction($t2)
		bne $t3, $t4, check_whether_register_mult
		beq $t2, 10, is_mult
		add $t2, $t2, 1
		add $t5, $t5, 1
		j while_check_mult
		
	#if not, check whether or * is proper register
	check_whether_register_mult:
		bne $t4, 42, end_check_mult
		
		move $s1, $ra
		move $v0, $t5
		jal check_proper_register
		move $ra, $s1
		beq $v0, 0, end_check_mult
		add $t5, $t5, 1
		add $t2, $t2, 1
		lb $t3, sentence($t5)
		beq $t3, 44, while_check_mult
		add $t5, $t5, 1
		j while_check_mult
		
	is_not_mult:
		li $v0, 0
		j end_check_mult
	is_mult:
		li $v0, 1
	end_check_mult:
		jr $ra
	
check_jump_register:
	li $t2, 0
	li $t5, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_jump_register:
		lb $t3, sentence($t5)
		lb $t4, jump_register_instruction($t2)
		bne $t3, $t4, check_whether_register_jump_register
		beq $t2, 4, is_jump_register
		add $t2, $t2, 1
		add $t5, $t5, 1
		j while_check_jump_register
		
	#if not, check whether or * is proper register
	check_whether_register_jump_register:	
		bne $t4, 42, end_check_jump_register
		
		move $s1, $ra
		move $v0, $t5
		jal check_proper_register
		move $ra, $s1
		beq $v0, 0, end_check_jump_register
		add $t5, $t5, 1
		add $t2, $t2, 1
		lb $t3, sentence($t5)
		beq $t3, 44, while_check_jump_register
		add $t5, $t5, 1
		j while_check_jump_register
	
	is_not_jump_register:
		li $v0, 0
		j end_check_jump_register
		
	is_jump_register:
		li $v0, 1
	end_check_jump_register:
		jr $ra
	
check_jump_and_link:
	li $t2, 0
	li $v0, 0
	
	#check whether each and every char is the same
	while_check_jump_and_link:
		lb $t3, sentence($t2)
		lb $t4, jump_and_link_instruction($t2)
		bne $t3, $t4, end_check_jump_and_link
		beq $t2, 3, is_jump_and_link
		add $t2, $t2, 1
		j while_check_jump_and_link
		
	is_jump_and_link:
		#check whether proper label has been provided at the end
		move $s1, $ra
		move $v0, $t2
		jal check_label
		move $ra, $s1
		
		beq $v0, 0, end_check_jump
		li $v0, 1
		
	end_check_jump_and_link:
		jr $ra
	
#checks whether register is between 0 and 31
check_proper_register:
	move $s2, $v0
	li $v0, 0
	lb $t6, sentence($s2)
	add $s2, $s2, 1
	lb $t7, sentence($s2)
	blt $t7, 48, one_digit #if next char is not a digit, we only have to check for proper one-digit register
	bgt $t7, 57, one_digit
	
		two_digits:	
			blt $t6, 49, end_check_register
			blt $t6, 51, proper_register
			bgt $t6, 51, end_check_register
			bgt $t7, 49, end_check_register
			j proper_register
		
		one_digit:
			blt $t6, 48, end_check_register
			bgt $t6, 57, end_check_register
		
		proper_register:
			li $v0, 1
			
		end_check_register:
			jr $ra

#checks whether provided value is a valid value
check_only_digits:
	move $s2, $v0
	add $s2, $s2, 1
	li $v0, 0
	
	lb $t6, sentence($s2)
	blt $t6, 49, end_check_only_digits
	bgt $t6, 57, end_check_only_digits
	
	while_check_only_digits:
		lb $t6, sentence($s2)
		add $s2, $s2, 1
		beq $t6, 10, only_digits_ok
		beq $t6, 0, only_digits_ok
		blt $t6, 48, end_check_only_digits
		bgt $t6, 57, end_check_only_digits
		j while_check_only_digits
		
	only_digits_ok:
		li $v0, 1
	end_check_only_digits:
		jr $ra
	
#checks whether provided label is a valid label, and exists
check_label:
	move $s2, $v0
	add $s2, $s2, 1
	li $v0, 0
	
	lb $t6, sentence($s2)
	blt $t6, 33, end_check_label
	bgt $t6, 126, end_check_label
	
	while_check_label:
		lb $t6, sentence($s2)
		add $s2, $s2, 1
		beq $t6, 0, label_ok
		beq $t6, 10, label_ok
		blt $t6, 33, end_check_label
		bgt $t6, 126, end_check_label
		j while_check_label
	
	label_ok:
		li $v0, 1
	end_check_label:
		jr $ra


#when instruction is confirmed to be valid, it's stored to the stack
add_whole_instruction_to_stack:
	move $s2, $ra
	
	jal find_sentences_length
	
	while_elements:
		jal find_elements_start
		jal save_element
		bge $v0, 0, while_elements
		
	move $ra, $s2
	jr $ra

#returns length of current instruction
find_sentences_length:
	li $t4, 0
	
	do_while_sentences_length:
		lb $t5, sentence($t4)
		beq $t5, 0, end_find_sentences_length
		beq $t5, 10, end_find_sentences_length
		add $t4, $t4, 1
		j do_while_sentences_length
	
	end_find_sentences_length:
		move $v0, $t4
		jr $ra

#given parameters end, finds its start
find_elements_start:
	move $t4, $v0
	
	do_while_this_element:
		lb $t5, sentence($t4)
		beq $t5, 32, end_find_elements_start
		subi $t4, $t4, 1
		beq $t4, 0, end_find_elements_start
		beq $t4, -1, end_find_elements_start
		j do_while_this_element
		
	end_find_elements_start:
		move $v1, $t4
		jr $ra

#saves single element to the stack
save_element:
	move $t4, $v1
	move $t5, $v0
	li $t6, 0
	bne $v1, 0, for_arguments
	
	for_inst_with_arguments:
		addi $t5, $t5, 1
		j copy_element
	
	for_arguments:
		addi $t4, $t4, 1
		j copy_element
	
	copy_element:
		lb $s1, sentence($t4)
		beq $s1, 10, rest
		sb $s1, stack_operator($t6)
		add $t6, $t6, 1
		add $t4, $t4, 1
		blt $t4, $t5, copy_element		
	
	rest:
		lw $t7, stack_operator
		addi $sp, $sp, -4
		sw $t7, 0($sp)
		addi $t9, $t9, 1
	
	li $s3, 0	
	li $s4, 0	
	clean_up_after_save:
		sb $s4, stack_operator($s3)
		add $s3, $s3, 1
		blt $s3, 4, clean_up_after_save
		
	move $v0, $v1
	subi $v0, $v0, 1
	
	jr $ra
	

#prints all elements from stack
print_elements:
	move $t8, $t9
	
	do_while_elements:
		lw $t7, 0($sp)
		addi $sp, $sp, 4
		sw $t7, stack_operator
		
		la $a0, new_line
		li $v0, 4
		syscall
		
		la $a0, stack_operator
		li $v0, 4
		syscall
		
		subi $t8, $t8, 1
		bgt $t8, 0, do_while_elements
		
	jr $ra
		
	
		
	